#/bin/bash
#
# This script converts the TDAQ software. It assumes it it started
# from the work directory and the TOKEN and GROUP files already exist.
# Only use this if you convert one of the standard TDAQ projects.
#

if [ ! -f PACKAGES ]
then
    echo "No PACKAGES file"
    exit 1
fi

if [  -z "${GITGROUP}" ]
then
    echo "No GITGROUP variable set"
    exit 2
fi

if [ -z "${GITTOKEN}" ]
then
    echo "No GITTOKEN variable set"
    exit 3
fi

if [ ! -f authors ]
then
    echo "No authors file"
    exit 4
fi

list_packages.sh
cp ${SVN2GIT_ROOT}/authors .
# get_authors.sh
convert-to-git.sh
create_projects.sh
push_all

