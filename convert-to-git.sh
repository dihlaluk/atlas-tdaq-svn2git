#!/bin/bash
#
# convert.sh [-r]
#
# Convert TDAQ SVN to Git.
#
# The script expects a file called PACKAGES in the current directory with the format:
#
# <package> <full_svn_path>
# 
# and the 'authors' file to map user names.
#
# It will create a directory 'svn2git_log' where all log files from the conversion
# will be stored
#
# If an output directory already exists, the correponding package will be not converted,
# except if the "-r" option is given. In this case a rebase of the existing conversion 
# happens.
#

logdir=`pwd`/svn2git_log
mkdir -p ${logdir} 
if [ $? -ne 0 ]; then
    echo "Cannot create logdir" 
    exit 1
fi

if [ ! -f ./PACKAGES ]
then
   echo "No PACKAGES file in current directory"
   exit 1
fi

if [ ! -f ./authors ]
then
   echo "No 'authors' file in current directory"
   exit 1
fi

rebase="true"
action="ignoring it"
if [ "${1}" == "-r" ]
then
    rebase="svn2git --rebase --authors ../authors"
    action="rebasing it"
fi

while read pkg svnroot
do
  echo ${pkg} ${svnroot}
  if [ -d ${pkg} ]
  then
      echo "Package already converted: ${pkg} - ${action}"
      (cd ${pkg}; ${rebase} < /dev/null)
  else
      echo "Converting ${pkg} for the first time"
      mkdir -p ${pkg} 
      if [ $? -ne 0 ]; then
          echo "Cannot create package directory ${pkg} in current working directory" 
          exit 1
      fi
      (cd ${pkg} && svn2git ${svnroot} --authors ../authors > ${logdir}/${pkg}.out 2> ${logdir}/${pkg}.err </dev/null)
  fi
  echo "Package ${pkg} done"
done < PACKAGES

