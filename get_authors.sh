#!/bin/bash
SVNROOT=${SVNROOT:=svn+ssh://svn.cern.ch/reps/atlastdaq}
if [ ! -f authors.in ]
then
    svn log --quiet  ${SVNROOT} | grep -E "r[0-9]+ \| .+ \|" | cut -d'|' -f2 | sed 's/ //g' | sort | uniq > authors.in
fi

for a in $(cat authors.in)
do
   mail_info=$(phonebook --login ${a} --terse login --terse displayname --terse email | sed 's/\([^;]*\);\([^;]*\);\([^;]*\);/\1 = \2 <\3>/')
   if [ ! -z "${mail_info}" ]
   then
       echo ${mail_info}
   else
       echo "${a} = ${a} <${a}@mail.cern.ch>"
   fi
done | sort -u > authors
echo "(no author) = nobody <nobody@nowhere.org>" >> authors

