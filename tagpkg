#!/bin/sh

# Usage: tagpkg [-l] <package> <release> [tag]
# Description:
# tagpkg <package> <release> [tag]
#
#
# Andrei.Kazarov,	Apr. 2002
# Last modified:	May. 2009
# Reiner Hauser         Jan  2016, GIT version

GITROOT=${GITROOT:=ssh://git@gitlab.cern.ch:7999}
GITGROUP=${GITGROUP:=atlas-tdaq-test}

TDAQ_DIR="/afs/cern.ch/atlas/project/tdaq"
collector_dir="${TDAQ_DIR}/cmt/adm/packages/tdaq"


usage()
{
echo "Usage: tagpkg [-l] [-q] <package> <release> [tag] "
echo "If -l given, lists the tag submitted for the release. Otherwise "
echo "-q: quiet, output only tag"
echo "commits a [tag] for a <package> to the <release> tag collector."
echo "NB: package name is given _without_ SVN prefix"
echo "Example:"
echo "tagpkg setup tdaq-01-06-00 setup-00-03-01"
echo "tagpkg dvs pre-01-06-00"
echo "tagpkg -l omni nightly"
exit 0
}

if test $# -lt 2 ; then
usage
fi

list=false
if test "$1" = "-l"; then list=true; shift; fi 

quiet=false
if test "$1" = "-q"; then quiet=true; shift; fi 

if test $# -lt 2 ; then
usage
fi

#echo $1 | grep -q '.*/.*'
#if test $? -ne 0; then
#   module=$container/$1
#else
#   module=$1
#fi
#package=`echo $1 | grep -o '[^/]*$'`

package=`basename $1`

release=$2

case $release in
	tdaq-common-*)
		collector_dir="${TDAQ_DIR}/cmt/adm/packages/tdaq-common"
		;;
	*dqm-common-*)
		collector_dir="${TDAQ_DIR}/cmt/adm/packages/dqm-common"
		;;
esac 

test -d $collector_dir/$release 		|| { echo "Error: No such release $release in the tag collector" ; exit 1 ; }
test -f $collector_dir/$release/$package 	|| { echo "Error: No such package $package in the release $release"; exit 1 ; }

container=`head -1 $collector_dir/$release/$package`
module=$container/$package

tag=`head -2 $collector_dir/$release/$package | tail -1`
if test "$tag" = "$container"; then tag="LATEST"; fi

if test "$list" = "true"; then
 if test "$quiet" = "true"; then
 	echo $tag
	exit 0
 fi
echo "Package: $module"
echo "Release: $release"
echo "Tag:     $tag"
exit 0
fi

#[ $release = "nightly.max" ] && { echo "nightly.max release frozen, please move your development to nightly"; exit 1; }

tag=$3
if test "$tag" = ""; then usage; fi

echo -n "Checking the tag ... "

if ! git ls-remote --tags ${GITROOT}/${GITGROUP}/${package} | grep "${tag}\$" 2>&1 > /dev/null
then
   echo "No such tag $tag for package $module found in git"
   exit 1
fi

echo OK

echo "Committing tag $tag for package $module for release $release ... " 

head -1 $collector_dir/$release/$package > $collector_dir/$release/_$package 	|| { echo "Failed. Tag collector is closed for $release ?" ; exit 1 ; }
echo $tag >> $collector_dir/$release/_$package 					|| { echo "Failed. Tag collector is closed for $release ?" ; exit 1 ; }
mv $collector_dir/$release/_$package $collector_dir/$release/$package 		|| { echo "Failed. Tag collector is closed for $release ?" ; exit 1 ; }
echo "OK."
